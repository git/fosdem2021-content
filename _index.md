---
title: PostgreSQL
themes:
- Database engines
website: https://www.postgresql.org/
logo: stands/postgresql/logo.png
chatroom: postgresql
description: |
    <p>PostgreSQL is a powerful, open source object-relational database system
    that uses and extends the SQL language combined with many features that safely
    store and scale the most complicated data workloads. </p>

    <p>The origins of PostgreSQL  date back to 1986 as part of the POSTGRES project at the University
    of California at Berkeley and has more than 30 years of active development on the core platform.  </p>

    <p>PostgreSQL has earned a strong reputation for its proven architecture, reliability,
    data integrity, robust feature set, extensibility, and the dedication of the open
    source community behind the software to consistently deliver performant and innovative
    solutions. </p>

    <p>PostgreSQL runs on all major operating systems, has been ACID-compliant
    since 2001, and has powerful add-ons such as the popular PostGIS geospatial database
    extender. </p>

    <p>It is no surprise that PostgreSQL has become the open source relational
    database of choice for many people and organisations. Getting started with
    using PostgreSQL has never been easier - pick a project you want to build, and
    let PostgreSQL safely and robustly store your data.</p>

showcase: |
    <link type=text/css rel="stylesheet" href="/stands/postgresql/style.css">
    <p>Come to our stand to meet the team behind PostgreSQL, get up to date with the latest developments, something you definitely do not want to miss!</p>
    <table class="table table-striped table-bordered table-condensed">
    <thead>
        <tr>
            <th colspan="2">Event</th>
            <th>Speakers</th>
            
            <th>Start</th>
            <th>End</th>
        </tr>
      </thead>
      <tbody>
      
    
        <tr>
          <td colspan="6"><h3>Sunday</h3></td>
        </tr>
        
          <tr>
            <td class="c3">&nbsp;</td>
            <td><a href="https://fosdem.org/2022/schedule/event/postgresql_solving_the_knapsack_problem_with_recursive_queries_and_postgresql/">Solving the knapsack problem with recursive queries and PostgreSQL</a></td>
            <td><a href="https://fosdem.org/2022/schedule/speaker/francesco_tisiot/" class="quiet">Francesco Tisiot</a></td>
              
            <td><a class="quiet value-title" title="2022-02-06T10:00:00+01:00" href="https://fosdem.org/2022/schedule/day/sunday/#1000">10:00</a></td>
            <td><a class="quiet value-title" title="2022-02-06T11:00:00+01:00" href="https://fosdem.org/2022/schedule/day/sunday/#1100">11:00</a></td>
          </tr>
        
          <tr>
            <td class="c4">&nbsp;</td>
            <td><a href="https://fosdem.org/2022/schedule/event/postgresql_json_in_postgresql_learning_with_a_case_study/">JSON in PostgreSQL - Learning with a case study</a></td>
            <td><a href="https://fosdem.org/2022/schedule/speaker/boriss_mejias/" class="quiet">Boriss Mejias</a></td>
              
            <td><a class="quiet value-title" title="2022-02-06T11:00:00+01:00" href="https://fosdem.org/2022/schedule/day/sunday/#1100">11:00</a></td>
            <td><a class="quiet value-title" title="2022-02-06T12:00:00+01:00" href="https://fosdem.org/2022/schedule/day/sunday/#1200">12:00</a></td>
          </tr>
        
          <tr>
            <td class="c5">&nbsp;</td>
            <td><a href="https://fosdem.org/2022/schedule/event/postgresql_how_to_create_lots_of_sample_time_series_data_with_postgresql_generate_series/">How to create (lots!) of sample time-series data with PostgreSQL generate_series()</a></td>
            <td><a href="https://fosdem.org/2022/schedule/speaker/ryan_booz/" class="quiet">Ryan Booz</a></td>
              
            <td><a class="quiet value-title" title="2022-02-06T12:00:00+01:00" href="https://fosdem.org/2022/schedule/day/sunday/#1200">12:00</a></td>
            <td><a class="quiet value-title" title="2022-02-06T12:30:00+01:00" href="https://fosdem.org/2022/schedule/day/sunday/#1230">12:30</a></td>
          </tr>
        
          <tr>
            <td class="c6">&nbsp;</td>
            <td><a href="https://fosdem.org/2022/schedule/event/postgresql_future_postgres_challenges/">Future Postgres Challenges</a></td>
            <td><a href="https://fosdem.org/2022/schedule/speaker/bruce_momjian/" class="quiet">Bruce Momjian</a></td>
              
            <td><a class="quiet value-title" title="2022-02-06T12:30:00+01:00" href="https://fosdem.org/2022/schedule/day/sunday/#1230">12:30</a></td>
            <td><a class="quiet value-title" title="2022-02-06T13:00:00+01:00" href="https://fosdem.org/2022/schedule/day/sunday/#1300">13:00</a></td>
          </tr>
        
          <tr>
            <td class="c7">&nbsp;</td>
            <td><a href="https://fosdem.org/2022/schedule/event/postgresql_postgresql_distributed_secure_database_ecosystem_building/">PostgreSQL Distributed &amp; Secure Database Ecosystem Building<br><i>This session will focus on introducing how to empower PostgreSQL thanks to the ecosystem provided by Apache ShardingSphere - an open source distributed database, plus an ecosystem users and developers need for their database to provide a customized and cloud-native experience.</i></a></td>
            <td><a href="https://fosdem.org/2022/schedule/speaker/juan_pan/" class="quiet">Juan  Pan</a></td>
              
            <td><a class="quiet value-title" title="2022-02-06T13:00:00+01:00" href="https://fosdem.org/2022/schedule/day/sunday/#1300">13:00</a></td>
            <td><a class="quiet value-title" title="2022-02-06T13:30:00+01:00" href="https://fosdem.org/2022/schedule/day/sunday/#1330">13:30</a></td>
          </tr>
        
          <tr>
            <td class="c8">&nbsp;</td>
            <td><a href="https://fosdem.org/2022/schedule/event/postgresql_automatically_refresh_materialized_views_in_postgresql/">Automatically refresh materialized views in PostgreSQL<br><i>Tactics to make refreshing a painless process</i></a></td>
            <td><a href="https://fosdem.org/2022/schedule/speaker/attila_toth/" class="quiet">Attila Tóth</a></td>
              
            <td><a class="quiet value-title" title="2022-02-06T13:30:00+01:00" href="https://fosdem.org/2022/schedule/day/sunday/#1330">13:30</a></td>
            <td><a class="quiet value-title" title="2022-02-06T14:00:00+01:00" href="https://fosdem.org/2022/schedule/day/sunday/#1400">14:00</a></td>
          </tr>
        
          <tr>
            <td class="c9">&nbsp;</td>
            <td><a href="https://fosdem.org/2022/schedule/event/postgresql_working_effectively_with_support_the_community/">Working effectively with (-support-) the community<br><i>This talk is for those who are new to PostgreSQL or those who just started, or all the others that want to hear a personal story: When I started with PostgreSQL around 10 years ago, I came with an Oracle background. It took me quite some time to understand how the PostgreSQL project is organized, how the community is working and how to deal with issues I've faced when I needed support. This is not a technical talk at all, but it should save you quite some time in your journey with PostgreSQL.</i></a></td>
            <td><a href="https://fosdem.org/2022/schedule/speaker/daniel_westermann/" class="quiet">Daniel Westermann</a></td>
              
            <td><a class="quiet value-title" title="2022-02-06T14:00:00+01:00" href="https://fosdem.org/2022/schedule/day/sunday/#1400">14:00</a></td>
            <td><a class="quiet value-title" title="2022-02-06T15:00:00+01:00" href="https://fosdem.org/2022/schedule/day/sunday/#1500">15:00</a></td>
          </tr>
        
          <tr>
            <td class="c10">&nbsp;</td>
            <td><a href="https://fosdem.org/2022/schedule/event/postgresql_exploring_linux_memory_usage_and_io_performance_for_cloud_native_databases/">Exploring Linux Memory Usage and IO Performance for Cloud Native Databases</a></td>
            <td><a href="https://fosdem.org/2022/schedule/speaker/fritshoogland/" class="quiet">FritsHoogland</a></td>
              
            <td><a class="quiet value-title" title="2022-02-06T15:00:00+01:00" href="https://fosdem.org/2022/schedule/day/sunday/#1500">15:00</a></td>
            <td><a class="quiet value-title" title="2022-02-06T16:00:00+01:00" href="https://fosdem.org/2022/schedule/day/sunday/#1600">16:00</a></td>
          </tr>
        
          <tr>
            <td class="c1">&nbsp;</td>
            <td><a href="https://fosdem.org/2022/schedule/event/postgresql_slow_things_down_to_make_them_go_faster/">Slow things down to make them go faster</a></td>
            <td><a href="https://fosdem.org/2022/schedule/speaker/jimmy_angelakos/" class="quiet">Jimmy Angelakos</a></td>
              
            <td><a class="quiet value-title" title="2022-02-06T16:00:00+01:00" href="https://fosdem.org/2022/schedule/day/sunday/#1600">16:00</a></td>
            <td><a class="quiet value-title" title="2022-02-06T17:00:00+01:00" href="https://fosdem.org/2022/schedule/day/sunday/#1700">17:00</a></td>
          </tr>
        
          <tr>
            <td class="c2">&nbsp;</td>
            <td><a href="https://fosdem.org/2022/schedule/event/postgresql_lesser_known_postgresql_features/">Lesser Known PostgreSQL Features<br><i>Features you already have but may not know about!</i></a></td>
            <td><a href="https://fosdem.org/2022/schedule/speaker/haki_benita/" class="quiet">Haki Benita</a></td>
              
            <td><a class="quiet value-title" title="2022-02-06T17:00:00+01:00" href="https://fosdem.org/2022/schedule/day/sunday/#1700">17:00</a></td>
            <td><a class="quiet value-title" title="2022-02-06T18:00:00+01:00" href="https://fosdem.org/2022/schedule/day/sunday/#1800">18:00</a></td>
          </tr>
        
      
    
     </tbody>
    </table>
  



new_this_year: |
  <p>PostgreSQL 14 was released in 30 September 2021, which makes it the latest
  major release.</p>
  <p>PostgreSQL 9.6 is no longer supported, if you still run on this (or
  an earlier) version, please come to us and let's talk about an upgrade strategy.</p>

layout: stand
---
